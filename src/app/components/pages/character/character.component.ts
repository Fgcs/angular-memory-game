import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CardComponent } from 'src/app/shared/components/card/card.component';
import { Character } from 'src/app/shared/models/character.model';
import { DataService } from 'src/app/shared/services/data.service';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {
  @ViewChildren(CardComponent)
  CardSComponent!: QueryList<CardComponent>;
  characters$ = this.dataService.characters$;
  totalFliped: number = 0;
  firtsCharacter: boolean = false;
  characterCard: any = null;
  characterMatch: any = null;
  win: boolean = false;
  constructor(private dataService: DataService,) { }
  ngOnInit(): void {
  }
  onSelect(character: Character, index: number) {
    if (!this.firtsCharacter) {
      this.characterCard = { ...character, index };

      this.firtsCharacter = true;

    } else {
      this.characterMatch = { ...character, index };
    }
    if (this.characterCard && this.characterMatch) {
      if (this.characterCard.id === this.characterMatch.id) {
        this.CardSComponent.get(this.characterCard.index)?.isMatch();
        this.CardSComponent.get(this.characterMatch.index)?.isMatch();
        this.totalFliped++;
        if (this.totalFliped === 15) {
          this.win = true;
        }
      } else {
        this.CardSComponent.get(this.characterCard.index)?.removeClass();
        this.CardSComponent.get(this.characterMatch.index)?.removeClass();

      }

      this.characterCard = null;
      this.characterMatch = null;
      this.firtsCharacter = false;
    }
  }
  onReset() {
    this.win = false;
    this.firtsCharacter = false;
    this.characterCard = null;
    this.characterMatch = null;
    this.totalFliped = 0;
    this.dataService.reset();
    this.CardSComponent.destroy();
  }
}
