import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharacterRoutingModule } from './character-routing.module';
import { CharacterComponent } from './character.component';
import { CardModule } from 'src/app/shared/components/card/card.module';
import { WinModule } from 'src/app/shared/components/win/win.module';


@NgModule({
  declarations: [
    CharacterComponent
  ],
  exports: [
    CharacterComponent
  ],
  imports: [
    CommonModule,
    CharacterRoutingModule,
    CardModule, WinModule
  ]
})
export class CharacterModule { }
