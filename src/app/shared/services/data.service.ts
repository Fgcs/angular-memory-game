import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { BehaviorSubject } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { Character } from '../models/character.model';
import { DataResponse } from '../models/data.interface';

@Injectable({
  providedIn: 'root'
})

export class DataService {
  private charactersSubject = new BehaviorSubject<Character[]>([]);
  characters$ = this.charactersSubject.asObservable();
  loading: boolean = true;
  constructor(private apollo: Apollo) {
    this.getDataApi();
  }

  private getDataApi() {
    //request apollo
    this.apollo.watchQuery<DataResponse>({
      query: this.generateQuery(this.generateRandom()),
    }).valueChanges.pipe(
      take(1),
      tap(({ data }) => {
        const { characters } = data;

        const charactersSlice = characters.results.slice(0, 15);
        const arrayConcact = charactersSlice.concat(charactersSlice);
        this.fisherYatesShuffle(arrayConcact);

        this.charactersSubject.next(arrayConcact);
        this.loading = false;
      })
    ).subscribe();

  }
  reset() {
    this.loading = true;
    this.getDataApi();
  }
  generateQuery(page: number) {
    return gql`
 {
   characters (page:${page}){
    results {
      id,
      name,
      image,
      status
    }
  }

}
 `;
  }
  fisherYatesShuffle(arr: Character[]) {
    for (var i = arr.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1)); //random index
      [arr[i], arr[j]] = [arr[j], arr[i]]; // swap
    }
  }
  generateRandom(min = 1, max = 42) {

    // find diff
    let difference = max - min;

    // generate random number
    let rand = Math.random();

    // multiply with difference
    rand = Math.floor(rand * difference);

    // add with min value
    rand = rand + min;

    return rand;
  }
}
