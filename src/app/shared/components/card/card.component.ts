import { DOCUMENT } from '@angular/common';
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { Character } from '../../models/character.model';
import { debounceTime } from 'rxjs';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() character!: Character;
  @Input() id: number = 0;
  @Output() characterSelect = new EventEmitter<Character>();
  constructor(@Inject(DOCUMENT) private document: Document) { }
  onSelect() {
    const card = this.document.getElementById(`${this.id}`);
    card?.classList.toggle('is-flipped');
    // this.characterSelect.emit(this.character);
    setTimeout(() => {
      this.characterSelect.emit(this.character);
    }, 800);

  }
  isMatch() {
    const card = this.document.getElementById(`${this.id}`);
    card?.classList.toggle('isMatch');
  }
  removeClass() {
    const card = this.document.getElementById(`${this.id}`);
    card?.classList.remove('is-flipped');
  }
}
