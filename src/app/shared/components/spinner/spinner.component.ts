import { Component } from '@angular/core';

@Component({
  selector: 'app-spinner',
  template: `
  <div class="overlay">
 <div class="lds-ripple"><div></div><div></div></div>
 <br>
  <span class="mt-3 text-spinner text-white">
    Cargando por favor espere...</span>
   </div>`,
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent {

}
