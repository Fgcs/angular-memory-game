import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-win',
  templateUrl: './win.component.html',
  styleUrls: ['./win.component.css']
})
export class WinComponent {
  @Output() reset = new EventEmitter<boolean>(false);

  onReset() {
    this.reset.emit(true);
  }
}
