import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WinComponent } from './win.component';



@NgModule({
  declarations: [
    WinComponent
  ],
  exports: [WinComponent],
  imports: [
    CommonModule
  ]
})
export class WinModule { }
