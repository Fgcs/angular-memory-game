import { Character } from "./character.model";

export interface DataResponse {
  characters: APIResponse<Character[]>;
}

export interface APIResponse<T> {
  results: T;
}
