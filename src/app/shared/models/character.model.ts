export class Character {
  id: number = 0;
  name: string = '';
  image: string = '';
  status: string = '';
  index: number = 0;
}

