
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { HomeModule } from './components/pages/home/home.module';
import { NotFoundModule } from './components/pages/notFound/not-found.module';
import { CharacterModule } from './components/pages/character/character.module';
import { AppRoutingModule } from './app-routing.module';
import { SpinnerModule } from './shared/components/spinner/spinner.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    GraphQLModule,
    HttpClientModule,
    HomeModule,
    NotFoundModule,
    CharacterModule,
    AppRoutingModule, SpinnerModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
