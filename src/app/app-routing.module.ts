import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
    path: '',
    loadChildren: () => import('./components/pages/home/home.module').then(m => m.HomeModule),
  },
  {
    path: 'not-found',
    loadChildren: () => import('./components/pages/notFound/not-found.module').then(m => m.NotFoundModule),
  },
  { path: '**', redirectTo: 'not-found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
