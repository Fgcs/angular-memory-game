/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {},
    container: {
      padding: '4rem',
      center: true,
    },

  },
  plugins: [],
}
